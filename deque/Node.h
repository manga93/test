#pragma once
#include <iostream>
using namespace std;

template <typename T>
class Node
{
private:
	T value;
	Node* next;
	Node* prev;
public:
	Node();
	Node(T value);
	Node(const Node<T>& obj);
	void operator= (const Node<T>& obj);
	friend ostream &operator << <> (ostream& output, const Node<T> &obj);
	bool operator == (const Node<T>& obj);
	bool operator < (const Node<T>& obj);
	bool operator <= (const Node<T>& obj);
	bool operator > (const Node<T>& obj);
	bool operator >= (const Node<T>& obj);
	bool operator != (const Node<T>& obj);
	Node<T>* getNext();
	Node<T>* getPrev();
	void setNext(Node<T>* obj);
	void setPrev(Node<T>* obj);
	~Node();
};



template <typename T>
Node<T>::Node()
{
	if (T == string) value = "";
	value = (T)0;
	next = nullptr;
	prev = nullptr;
}

template<typename T>
Node<T>::Node(T value)
{
	this->value = value;
	next = nullptr;
	prev = nullptr;
}

template<typename T>
Node<T>::Node(const Node<T>& obj)
{
	value = obj.value;
	next = obj.next;
	prev = obj.prev;
}

template<typename T>
void Node<T>::operator=(const Node<T>& obj)
{
	value = obj.value;
	next = obj.next;
	prev = obj.prev;
}

template<typename T>
ostream & operator<<(ostream & output, const Node<T>& obj)
{
	output << "Value: " << obj.value << " Next: ";
	if (obj.next) output << obj.next->value;
	else output << obj.next;
	output << " Prev: ";
	if (obj.prev) output << obj.prev->value;
	else output << obj.prev;
	output << endl;
	return output;
}

template<typename T>
bool Node<T>::operator==(const Node<T>& obj)
{
	return value == obj.value;
}

template<typename T>
bool Node<T>::operator<(const Node<T>& obj)
{
	return value < obj.value;
}

template<typename T>
bool Node<T>::operator<=(const Node<T>& obj)
{
	return value <= obj.value;
}

template<typename T>
bool Node<T>::operator>(const Node<T>& obj)
{
	return value > obj.value;
}

template<typename T>
bool Node<T>::operator>=(const Node<T>& obj)
{
	return value >= obj.value;
}

template<typename T>
bool Node<T>::operator!=(const Node<T>& obj)
{
	return value != obj.value;
}

template<typename T>
Node<T>* Node<T>::getNext()
{
	return next;
}

template<typename T>
Node<T>* Node<T>::getPrev()
{
	return prev;
}

template<typename T>
void Node<T>::setNext(Node<T>* obj)
{	
	next = obj;
}

template<typename T>
void Node<T>::setPrev(Node<T>* obj)
{	
	prev = obj;
}

template<typename T>
Node<T>::~Node()
{
}
