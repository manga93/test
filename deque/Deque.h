#pragma once
#include"Node.h"

template<typename T>
class Deque
{
private:
	Node<T>* front;
	Node<T>* back;
	int size;
public:
	Deque();
	bool isEmpty();
	Node<T>* getBack() const;
	Node<T>* getFront() const;
	void pushFront(Node<T>& obj);
	void pushBack(Node<T>& obj);
	void popFront();
	void popBack();
	void printAll();
	void popAll();
	void emplaceFront(int value);
	void emplaceBack(int value);
	int getSize();
	~Deque();
};

template<typename T>
Deque<T>::Deque()
{
	front = nullptr;
	back = nullptr;
	size = 0;
}

template<typename T>
bool Deque<T>::isEmpty()
{
	return size == 0;
}

template<typename T>
Node<T>* Deque<T>::getBack() const
{
	return back;
}

template<typename T>
Node<T>* Deque<T>::getFront() const
{
	return front;
}

template<typename T>
void Deque<T>::pushFront(Node<T>& obj)
{
	Node<T>* temp = new Node<T>(obj);
	if (front == nullptr) {
		front = temp; 
		back = temp;
		++size;
	}
	else {
		front->setNext(temp);
		temp->setPrev(front);
		front = temp;
		++size;
	}
}

template<typename T>
void Deque<T>::pushBack(Node<T>& obj)
{
	Node<T>* temp = new Node<T>(obj);
	if (front == nullptr) {
		front = temp;
		back = temp;
		++size;
	}
	else {
		back->setPrev(temp);
		temp->setNext(back);
		back = temp;
		++size;
	}
}

template<typename T>
void Deque<T>::popFront()
{
	if (front) {
		Node<T>* temp = front;
		front = front->getPrev();
		front->setNext(nullptr);
		--size;
		delete temp;
	}
}

template<typename T>
void Deque<T>::popBack()
{
	if (back) {
		Node<T>* temp = back;
		back = back->getNext();
		back->setPrev(nullptr);
		--size;
		delete temp;
	}
}

template<typename T>
void Deque<T>::printAll()
{
	cout << endl;
	if (front) {
		Node<T>* temp = front;
		while (temp) {
			cout << *temp << endl;
			temp = temp->getPrev();
		}
	}
	else cout << "Deque is empty" << endl;
}

template<typename T>
void Deque<T>::popAll()
{
	Node<T>* temp = front;
	while (temp) {
		temp = front->getPrev();
		delete front;
		front = temp;
		--size;
	}
	back = nullptr;
}

template<typename T>
void Deque<T>::emplaceFront(int value)
{	
	Node<T>* temp = new Node<T>(value);
	pushFront(*temp);
}

template<typename T>
void Deque<T>::emplaceBack(int value)
{	
	Node<T>* temp = new Node<T>(value);
	pushBack(*temp);
}

template<typename T>
int Deque<T>::getSize()
{
	return size;
}

template<typename T>
Deque<T>::~Deque()
{
	Node<T>* temp = front;
	while (temp) {
		temp = front->getPrev();
		delete front;
		front = temp;
		--size;
	}
}
