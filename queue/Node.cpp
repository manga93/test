#include "stdafx.h"
#include "Node.h"

Node::Node()
{
	value = 0;
	next = nullptr;
}

Node::Node(int new_value)
{
	value = new_value;
	next = nullptr;
}

Node::Node(const Node & obj)
{
	value = obj.value;
	next = obj.next;
}

void Node::operator=(const Node & obj)
{
	value = obj.value;
	next = obj.next;
}

bool Node::operator<(const Node & obj)
{
	return value < obj.value;
}

bool Node::operator>(const Node & obj)
{
	return value > obj.value;
}

bool Node::operator<=(const Node & obj)
{
	return value <= obj.value;
}

bool Node::operator>=(const Node & obj)
{
	return value >= obj.value;
}

bool Node::operator==(const Node & obj)
{
	return value == obj.value;
}

bool Node::operator!=(const Node & obj)
{
	return value != obj.value;
}

Node Node::operator++()
{
	return ++value;
}

Node Node::operator++(int)
{
	Node temp(value);
	++value;
	return temp;
}

Node Node::operator--()
{
	return --value;
}

Node Node::operator--(int)
{
	Node obj(value);
	--value;
	return obj;
}

Node Node::operator+(const Node & obj)
{
	Node temp;
	temp.value = this->value + obj.value;
	return temp;
}

Node Node::operator-(const Node & obj)
{
	Node temp;
	temp.value = this->value - obj.value;
	return temp;
}

Node Node::operator*(const Node & obj)
{
	Node temp;
	temp.value = this->value * obj.value;
	return temp;
}

Node Node::operator/(const Node & obj)
{
	Node temp;
	temp.value = this->value / obj.value;
	return temp;
}

Node Node::operator%(const Node & obj)
{
	Node temp;
	temp.value = this->value % obj.value;
	return temp;
}

int Node::getValue() const 
{
	return value;
}

void Node::setValue(int new_value)
{
	value = new_value;
}

void Node::setNext( Node* obj)
{
	next = obj;
}

Node * Node::getNext()
{
	return next;
}

Node::~Node()
{
}

ostream & operator << (ostream & output, const Node & obj)
{
	output << "Value: " << obj.value << " Next: ";
	if (obj.next) output << obj.next->value;
	else output << obj.next;
	return output;
}


