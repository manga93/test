#pragma once
#include <iostream>
using namespace std;

class Node
{
private:
	int value;
	Node* next;
public:
	Node();
	Node(int new_value);
	Node(const Node& obj);
	void operator= (const Node& obj);
	bool operator< (const Node& obj);
	bool operator> (const Node& obj);
	bool operator<= (const Node& obj);
	bool operator>= (const Node& obj);
	bool operator== (const Node& obj);
	bool operator!= (const Node& obj);
	Node operator++();
	Node operator++(int);
	Node operator--();
	Node operator--(int);
	Node operator+(const Node& obj);
	Node operator-(const Node& obj);
	Node operator*(const Node& obj);
	Node operator/(const Node& obj);
	Node operator%(const Node& obj);
	friend ostream &operator << (ostream& output, const Node &obj);
	int getValue() const;
	void setValue(int new_value);
	void setNext(Node* obj);
	Node* getNext();
	~Node();
};

