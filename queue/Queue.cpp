#include "stdafx.h"
#include "Queue.h"


Queue::Queue()
{
	front = nullptr;
	back = nullptr;
	size = 0;
}

bool Queue::isEmpty()
{
	return size == 0;
}

Node * Queue::getFront()
{
	return front;
}

Node * Queue::getBack()
{
	return back;
}

void Queue::pushBack(Node& obj)
{
	Node* temp = new Node(obj);
	if (front == nullptr) {
		front = temp;
		back = temp;
		++size;
	}
	else {
		back->setNext(temp);
		back = temp;
		++size;
	}
}

void Queue::printAll()
{
	cout << endl;
	if (front) {
		Node* temp = front;
		int temp_size = size;
		while (temp_size != 0) {
			cout << *temp << endl;
			temp = temp->getNext();
			--temp_size;
		}
	}
	else cout << "Queue is empty" << endl;
}

void Queue::emplaceNode(int value)
{
	Node* temp = new Node(value);
	pushBack(*temp);
}

void Queue::popFront()
{
	if (front != nullptr) {
		front = front->getNext();
		--size;
	}
}

void Queue::popAll()
{
	Node* temp = front;
	while (temp) {
		temp = temp->getNext();
		delete front;
		front = temp;
		--size;
	}
}

int Queue::getSize() const
{
	return size;
}


Queue::~Queue()
{
	Node* temp = front;
	while (temp) {
		temp = temp->getNext();
		delete front;
		front = temp;
		--size;
	}
}
