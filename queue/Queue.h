#pragma once

#include "Node.h"

class Queue
{
private:
	Node* front;
	Node* back;
	int size;
public:
	Queue();
	bool isEmpty();
	Node* getFront();
	Node* getBack();
	void pushBack(Node& obj);
	void printAll();
	void emplaceNode(int value);
	void popFront();
	void popAll();
	int getSize() const;
	~Queue();
};

